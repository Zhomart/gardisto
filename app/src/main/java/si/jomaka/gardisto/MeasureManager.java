package si.jomaka.gardisto;

/**
 * Created by zhomart on 12/4/14.
 */
public class MeasureManager {
    // length of conveyor = conveyor_update_interval * conveyor_max_measures milli-seconds
    Conveyor conveyor;

    // Make 6 seconds
    long conveyor_update_interval = 200; // in milli-seconds, every 250 milli-sec
    int conveyor_max_measures = 30; // max length of conveyor


    public MeasureManager(){
        conveyor = new Conveyor(conveyor_max_measures, conveyor_update_interval);
    }

    public Conveyor getConveyor(){ return conveyor; }

    public void addMeasure(Measure m){
        conveyor.add(m);
    }

    public void addConveyorEventHandler(ConveyorEventHandler conveyorEvent) {
        conveyor.addEventHandler(conveyorEvent);
    }
}
