package si.jomaka.gardisto.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import si.jomaka.gardisto.DangerRecognizer;
import si.jomaka.gardisto.DangerRecognizerEventHandler;
import si.jomaka.gardisto.GPSTracker;
import si.jomaka.gardisto.MeasureManager;
import si.jomaka.gardisto.MainActivity;
import si.jomaka.gardisto.Measure;

/**
 * Created by zhomart on 11/28/14.
 */
public class MainService extends Service implements DangerRecognizerEventHandler {
    MainWorker mainWorker;
    Thread thread;

    // Make POST request
    // public static String alertURI = "http://10.0.0.4:3000/dangers";
    public static String alertURI = "http://gardisto.herokuapp.com/dangers";

    private SensorManager mSensorManager;
    private Sensor mSensor;

    private SensorEventListener mEventListenerAccelerometer;
    long prevMillis = 0;

    public static MainActivity mainActivity;
    public static MainService instance;

    public MeasureManager intervalManager;

    public DangerRecognizer dangerRecognizer;

    public long lastDangerAt = 0;

    Location last_location = null;
    long last_location_retrieved_at = 0;

    LocationManager locationManager;
    LocationListener locationListener;

    GPSTracker gps;


    @Override
    public void onCreate() {
        instance = this;

        intervalManager = new MeasureManager();

        mainWorker = new MainWorker(this);

        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);

        initListeners();

        thread = new Thread(mainWorker);

        dangerRecognizer = new DangerRecognizer(intervalManager.getConveyor());
        dangerRecognizer.addEventHandler(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!isWorkerRunning())
            startWorker();

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (isWorkerRunning())
            stopWorker();
    }

    void setupLocationListener(){
        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
       locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                makeUseOfNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    void disableLocationListener(){
        locationManager.removeUpdates(locationListener);
    }

    void makeUseOfNewLocation(Location location){
        Log.i("Location-time", (System.currentTimeMillis() - last_location_retrieved_at)/1000.0 + "");
        Log.i("Location", location.toString());
        this.last_location = location;
        last_location_retrieved_at = System.currentTimeMillis();
    }

    void startWorker(){
        // Toast.makeText(this, "service: starting worker", Toast.LENGTH_SHORT).show();

        if (mainActivity != null)
            mainActivity.setWorkerSwitch(true);

        mainWorker.control_is_running = true;
        // thread.start();

        mSensorManager.registerListener(mEventListenerAccelerometer, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        setupLocationListener();

        gps = new GPSTracker(this);
    }

    void stopWorker(){
        // Toast.makeText(this, "service: stopping worker", Toast.LENGTH_SHORT).show();

        if (mainActivity != null)
            mainActivity.setWorkerSwitch(false);

        mainWorker.control_is_running = false;

        mSensorManager.unregisterListener(mEventListenerAccelerometer);

        disableLocationListener();

        gps.stopUsingGPS();
    }

    public boolean isWorkerRunning(){
        return thread != null && thread.isAlive();
    }

    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    float[] gravity = new float[3];
    final float alpha = 0.8f;

    private void initListeners() {
        mEventListenerAccelerometer = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                // Isolate the force of gravity with the low-pass filter.
                gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
                gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
                gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

                long curMillis = System.currentTimeMillis();
                long diff = curMillis - prevMillis;
                prevMillis = curMillis;

                Measure m = new Measure(curMillis);
                m.setAccel(event.values[0] - gravity[0], event.values[1] - gravity[1], event.values[2] - gravity[2]);
                intervalManager.addMeasure(m);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
    }

    public void onDangerRecognized(float dangerLevel, float[] ary){
        double longitude = gps.getLongitude();
        double latitude = gps.getLatitude();

        new RequestTask().execute(alertURI, dangerLevel + "", longitude + "", latitude + "");

        Log.i("MainService", "danger recognized " + dangerLevel);
        lastDangerAt = System.currentTimeMillis();
        if (mainActivity != null && mainActivity.active) {
            mainActivity.checkDanger();
            return;
        }

        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void onDangerLevelComputed(float dangerLevel, float[] ary){
    }

}

class RequestTask extends AsyncTask<String, String, String>{

    @Override
    protected String doInBackground(String... args) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
        HttpPost httppost = new HttpPost(args[0]);

        String name = Build.MODEL;

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("level", args[1]));
            nameValuePairs.add(new BasicNameValuePair("name", name));
            nameValuePairs.add(new BasicNameValuePair("longitude", args[2]));
            nameValuePairs.add(new BasicNameValuePair("latitude", args[3]));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            response = httpclient.execute(httppost);
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            //TODO Handle problems..
        } catch (IOException e) {
            //TODO Handle problems..
        }
        return responseString;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        //Do anything with response..

        Log.i("MainService", "HTTP Response: " + result);
    }
}