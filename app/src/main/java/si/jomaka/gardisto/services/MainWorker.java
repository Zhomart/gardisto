package si.jomaka.gardisto.services;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by zhomart on 11/28/14.
 */
public class MainWorker implements Runnable {
    android.app.Service service;

    public boolean control_is_running;


    public MainWorker(android.app.Service service){
        this.service = service;
    }

    // @Override
    public void run() {
        Log.d("Worker", "at top of the run");
        // Toast.makeText(service, "worker: started", Toast.LENGTH_SHORT).show();

        while (control_is_running){
            Log.d("Worker", "iter");

            try {
                Thread.sleep(1000l);
            }catch(InterruptedException ex){
                break;
            }
        }

        Log.d("Worker", "at end of the run");

        // Toast.makeText(service, "worker: stopped", Toast.LENGTH_SHORT).show();
    }
}
