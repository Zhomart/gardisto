package si.jomaka.gardisto;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zhomart on 12/9/14.
 */
public class Conveyor {
    int max_measures; // max length of conveyor

    long lastUpdateTS;

    long updateInterval; // evey  milli-sec

    List<Measure> raw_measures;

    public List<Measure> measures;

    List<ConveyorEventHandler> eventHandlers;


    public Conveyor(int max_measures, long updateInterval){
        this.max_measures = max_measures;
        this.updateInterval = updateInterval;

        lastUpdateTS = 0;

        raw_measures = new LinkedList<Measure>();
        measures = new LinkedList<Measure>();

        eventHandlers = new ArrayList<ConveyorEventHandler>();
    }

    public void add(Measure m){
        if (lastUpdateTS == 0) lastUpdateTS = m.getTs();

        raw_measures.add(m);

        if (m.getTs() - lastUpdateTS >= updateInterval){
            updateMeasuring();
        }
    }

    void updateMeasuring(){
        Measure last = raw_measures.get(raw_measures.size() - 1);
        Measure avg = new Measure(last.getTs());

        for (Measure m : raw_measures){
            avg.add(m);
        }

        avg.divide((float)raw_measures.size());

        measures.add(avg);

        while (measures.size() > max_measures)
            measures.remove(0);

        raw_measures = new LinkedList<Measure>();
        if (last.getTs() > lastUpdateTS + updateInterval)
            raw_measures.add(last);

        if (measures.size() == max_measures)
            for (ConveyorEventHandler e : eventHandlers)
                e.onConveyorReady(this);

        lastUpdateTS = last.getTs();
    }

    List<Measure> getMeasures(){
       return measures;
    }

    public void addEventHandler(ConveyorEventHandler e){
        for (ConveyorEventHandler handler : eventHandlers)
            if (handler == e) return;
        eventHandlers.add(e);
    }
}

interface ConveyorEventHandler {
    public void onConveyorReady(Conveyor conveyor);
}
