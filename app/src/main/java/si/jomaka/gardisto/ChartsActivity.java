package si.jomaka.gardisto;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import si.jomaka.gardisto.services.MainService;


public class ChartsActivity extends Activity {

    GraphView graphView;
    GraphViewSeries exampleSeries;

    long started_at = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts);
    }

    public void onStart(){
        super.onStart();

        // init example series data
        exampleSeries = new GraphViewSeries(new GraphView.GraphViewData[] {
                new GraphView.GraphViewData(1, 2.0d)
                , new GraphView.GraphViewData(2, 1.5d)
                , new GraphView.GraphViewData(3, 2.5d)
                , new GraphView.GraphViewData(4, 1.0d)
        });

        graphView = new LineGraphView(this, "Graph");
        graphView.addSeries(exampleSeries); // data
        graphView.setManualMinY(true);
        graphView.setManualYMinBound(-20);
        graphView.setManualYMaxBound(20);

        started_at = System.currentTimeMillis();

        final GraphViewSeries.GraphViewSeriesStyle style_x = new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(200, 50, 100), 2);
        final GraphViewSeries.GraphViewSeriesStyle style_y = new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(90, 250, 0), 2);
        final GraphViewSeries.GraphViewSeriesStyle style_z = new GraphViewSeries.GraphViewSeriesStyle(Color.rgb(0, 50, 100), 2);

        if (false && MainService.instance != null){
            MainService.instance.intervalManager.addConveyorEventHandler(new ConveyorEventHandler() {
                @Override
                public void onConveyorReady(Conveyor conveyor) {
                    long cur = System.currentTimeMillis();
                    Log.d("Charts", "conveyor ready: " + (cur - started_at) / 1000.0);
                    started_at = cur;

                    GraphView.GraphViewData[] ary_x = new GraphView.GraphViewData[conveyor.measures.size()];
                    GraphView.GraphViewData[] ary_y = new GraphView.GraphViewData[conveyor.measures.size()];
                    GraphView.GraphViewData[] ary_z = new GraphView.GraphViewData[conveyor.measures.size()];
                    int index = 0;
                    for (Measure m : conveyor.measures) {
                        ary_x[index] = new GraphView.GraphViewData(index, m.accelX);
                        ary_y[index] = new GraphView.GraphViewData(index, m.accelY);
                        ary_z[index] = new GraphView.GraphViewData(index, m.accelZ);
                        index++;
                    }

                    graphView.removeAllSeries();

                    graphView.addSeries(new GraphViewSeries("X", style_x, ary_x));
                    graphView.addSeries(new GraphViewSeries("Y", style_y, ary_y));
                    graphView.addSeries(new GraphViewSeries("Z", style_z, ary_z));
                }
            });
        }

        if (MainService.instance != null){
            MainService.instance.dangerRecognizer.addEventHandler(new DangerRecognizerEventHandler() {
                @Override
                public void onDangerRecognized(float dangerLevel, float[] ary) {
                    Log.i("Danger", "DANGER with level " + dangerLevel);
                }

                @Override
                public void onDangerLevelComputed(float dangerLevel, float[] ary) {
                    GraphView.GraphViewData[] ary_x = new GraphView.GraphViewData[ary.length];
                    for (int i=0;i<ary.length;++i) {
                        ary_x[i] = new GraphView.GraphViewData(i, ary[i]);
                    }

                    graphView.removeAllSeries();

                    graphView.addSeries(new GraphViewSeries("Danger", style_x, ary_x));
                }

            });
        }

        LinearLayout layout = (LinearLayout) findViewById(R.id.graph1);
        layout.addView(graphView);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.charts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
