package si.jomaka.gardisto;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhomart on 12/9/14.
 */
public class DangerRecognizer implements ConveyorEventHandler {
    Conveyor conveyor;

    List<DangerRecognizerEventHandler> eventHandlers;
    float[] y = new float[0];

    float[] dangerLevels = new float[10];

    public DangerRecognizer(Conveyor conveyor){
        this.conveyor = conveyor;

        conveyor.addEventHandler(this);

        eventHandlers = new ArrayList<DangerRecognizerEventHandler>();
    }

    public void addEventHandler(DangerRecognizerEventHandler eventHandler) {
        eventHandlers.add(eventHandler);
    }

    public void onConveyorReady(Conveyor c){
        y = new float[c.measures.size()];
        int index = 0;
        for (Measure m : c.measures)
            y[index++] = (float)Math.sqrt(m.accelX * m.accelX + m.accelY * m.accelY + m.accelZ * m.accelZ);

        float[] scores = new float[5];

        for (int ind=0, j=0; ind<5; ind++, j+= 6){
            float sum = 0;
            if (j == 0)
                sum += y[j] + 3 * y[j + 1] + y[j + 2];
            else if (j == y.length -1 )
                sum += y[j - 2] + 3 * y[j - 1] + y[j];
            else
                sum += y[j-1] + 3 * y[j] + y[j+1];
            scores[ind] = sum / 5.0f;
        }

        float dangerLevel = computeDangerLevel(scores);

        for (int i=0;i<dangerLevels.length - 1;++i)
            dangerLevels[i] = dangerLevels[i + 1];
        dangerLevels[dangerLevels.length - 1] = dangerLevel;

        Log.d("DangerLevel", " = " + dangerLevel);

        for (DangerRecognizerEventHandler e : eventHandlers)
            e.onDangerLevelComputed(dangerLevel, scores);

        if (dangerLevel > 15)
            for (DangerRecognizerEventHandler e : eventHandlers)
                e.onDangerRecognized(dangerLevel, scores);
    }

    //
    // For training info look at work/ml/week2
    //
    float computeDangerLevel(float[] x){
        int n = 5;

        float[] theta = {6.604283f, -1.290500f, 4.008853f, -0.053135f, -0.439686f, -1.169893f};
        float[] mu = {2.620000f, 3.180000f, 2.155000f, 4.228550f, 1.923504f};
        float[] sigma = {7.080000f, 2.375000f, 3.124032f, 2.450263f, 3.609268f};

        for (int i=0;i<n;++i)
            x[i] = (x[i] - mu[i]) / sigma[i];

        // Add extra 1 to the beginning of x
        float result = theta[0];
        for (int i=0;i<n;++i)
            result += x[i] * theta[i + 1];

        return result;
    }


}
