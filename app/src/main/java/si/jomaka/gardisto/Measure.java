package si.jomaka.gardisto;

/**
 * Created by zhomart on 12/4/14.
 */
public class Measure {
    long ts;

    float accelX, accelY, accelZ;

    public Measure(long ts){
        this.ts = ts;
    }

    public long getTs(){ return ts; }

    public void setAccel(float x, float y, float z){
        accelX = x;
        accelY = y;
        accelZ = z;
    }

    public Measure add(Measure m){
        this.accelX += m.accelX;
        this.accelY += m.accelY;
        this.accelZ += m.accelZ;
        return this;
    }

    public Measure divide(float number){
        this.accelX /= number;
        this.accelY /= number;
        this.accelZ /= number;
        return this;
    }

}
