package si.jomaka.gardisto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhomart on 12/4/14.
 */
public class Interval {
    long length; // in milli-seconds
    int max_measures; // number of measures
    long startedTS;

    List<Measure> raw_measures;
    List<Measure> measures;

    boolean finished;

    int index;

    public Interval(long length, int max_measures, long currentTS){
        this.length = length;
        this.max_measures = max_measures;
        this.startedTS = currentTS;
        raw_measures = new ArrayList<Measure>();
        finished = false;
        index = 0;
    }

    public boolean isFinished(){
        return finished;
    }

    public void add(Measure m){
        if (isFinished())
            return;

        raw_measures.add(m);

        if (m.ts - startedTS > length){
            finishMeasuring();
        }
    }

    void finishMeasuring(){
        if (raw_measures.size() < max_measures){
            measures = raw_measures;
        } else {
            measures = new ArrayList<Measure>();
            int step = raw_measures.size() / max_measures;
            for (int i=0, k = 0;i<max_measures;++i){
                measures.add(raw_measures.get(k));
                k += step;
            }
        }
        finished = true;
    }

    List<Measure> getMeasures(){
        if (isFinished())
            return measures;
        else
            return null;
    }

}
