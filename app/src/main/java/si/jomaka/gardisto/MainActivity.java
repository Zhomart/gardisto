package si.jomaka.gardisto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.*;
// import org.openintents.sensorsimulator.hardware.*;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;

import si.jomaka.gardisto.services.MainService;


public class MainActivity extends Activity {
    Button helpMeButton;
    Button cancelButton;
    CheckBox workerSwitch;

    Intent mainServiceIntent;

    public boolean active;

    long lastDangerAt = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onStart(){
        super.onStart();

        MainService.mainActivity = this;

        mainServiceIntent = new Intent(this, MainService.class);
        startService(mainServiceIntent);

        helpMeButton = (Button)findViewById(R.id.help_me_button);
        cancelButton = (Button)findViewById(R.id.cancel_button);
        workerSwitch = (CheckBox)findViewById(R.id.switch_worker);

        setupEvents();
    }

    public void checkDanger(){
        Log.i("MainActivity", "====> 1");
        if (MainService.instance == null)
            return;
        Log.i("MainActivity", "====> 2");
        if (System.currentTimeMillis() - lastDangerAt < 20000)
            return;

        Log.i("MainActivity", "====> 2.5");

        if (System.currentTimeMillis() - MainService.instance.lastDangerAt > 20000)
            return;

        Log.i("MainActivity", "====> 3");

        lastDangerAt = System.currentTimeMillis();

        playAlarm();
        Log.i("MainActivity", "====> 4");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (MainService.instance != null)
            setWorkerSwitch(MainService.instance.isWorkerRunning());

        active = true;
        checkDanger();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("onPause", "unregistered from sensors");
        active = false;
    }

    void setupEvents(){
        workerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    startService(mainServiceIntent);
                else
                    stopService(mainServiceIntent);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                // System.exit(0);
            }
        });

        helpMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float[] ary = {};
                MainService.instance.onDangerRecognized(25, ary);
            }
        });
    }

    public void setWorkerSwitch(boolean value){
        workerSwitch.setChecked(value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_charts){
            startActivity(new Intent(this, ChartsActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void playAlarm(){
        Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000};
        v.vibrate(pattern, -1);

        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alarm);
        mediaPlayer.start();
    }
}
