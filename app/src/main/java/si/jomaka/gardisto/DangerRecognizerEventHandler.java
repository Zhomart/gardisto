package si.jomaka.gardisto;

/**
 * Created by zhomart on 12/9/14.
 */
public interface DangerRecognizerEventHandler {
    public void onDangerRecognized(float dangerLevel, float[] ary);
    public void onDangerLevelComputed(float dangerLevel, float[] ary);
}